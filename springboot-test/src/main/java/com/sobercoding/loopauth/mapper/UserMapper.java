//package com.sobercoding.loopauth.mapper;
//
//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import com.fasterxml.jackson.databind.ser.Serializers;
//import com.sobercoding.loopauth.model.entity.User;
//
///**
// * @program: LoopAuth
// * @author: Sober
// * @Description:
// * @create: 2022/08/09 15:54
// */
//public interface UserMapper extends BaseMapper<User> {
//}
